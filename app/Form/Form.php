<?php
namespace App\Form;

//
class Form {
	public static $outputJs = NULL;

	public function __construct() {

	}

	//
	public static function htmlInput($config) {
		//htmlInputValidate($config);

		$id 	= (isset($config['htmlOptions']['id'])) ? $config['htmlOptions']['id'] : NULL;
		$name 	= (isset($config['name'])) ? $config['name'] : NULL;
		$label 	= $config['label']['content'];
		$value 	= (isset($config['value'])) ? $config['value'] : NULL;

		if (in_array($config['type'], array("text", "textarea", "select"))) {
			$config['htmlOptions']['class'] = (isset($config['htmlOptions']['class'])) ? 'form-control ' . $config['htmlOptions']['class'] : 'form-control';
		}

		$htmlOptions = (isset($config['htmlOptions'])) ? self::makeHTMLTags($config['htmlOptions']) : NULL;
		$htmlString  = (isset($config['htmlString'])) ? $config['htmlString'] : NULL;

		/*
		print "<pre>";
		print_r($htmlOptions);
		print "</pre>";
		exit;
		*/


		$isError = (isset($config['errors'])) ? $config['errors'] : NULL;
		$isError = ($isError) ? 'has-error' : NULL;

		$output = "
			<div class='form-group $isError'>
			";

		switch ($config['type']) {
			case 'money':
				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<input type='text' name='$name' value='$value' $htmlOptions $htmlString>";

				self::$outputJs .= "$( '#$id' ).maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});";

				break;
			case 'datepicker':
				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<input type='text' name='$name' value='$value' $htmlOptions $htmlString>";

				self::$outputJs .= "$( '#$id' ).datepicker();";

				break;
			case 'cpf':
				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<input type='text' name='$name' value='$value' $htmlOptions $htmlString>";

				self::$outputJs .= "$('#$id').mask('999.999.999-99')";

				break;
			case 'cnpj':
				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<input type='text' name='$name' value='$value' $htmlOptions $htmlString>";

				self::$outputJs = "$('#$id').mask('99.999.999/9999-99')";

				break;				
			case 'text':
				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<input type='text' name='$name' value='$value' $htmlOptions $htmlString>";

				break;
			case 'textarea':
				$rows = (isset($config['htmlOptions']['row'])) ? $config['htmlOptions']['row'] : 3;

				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<textarea name='$name' rows='$rows' $htmlOptions $htmlString>$value</textarea>";

				break;
			case 'select':
				$listData = $config['listData'];
				$selectedValue = (isset($config['selected'])) ? $config['selected'] : NULL;

				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<select name='$name' $htmlOptions $htmlString>";

				$output .= "<option value=''>Selecione</option>";
				foreach($listData as $id => $listDataValue) {
					$selected = ($selectedValue == $id) ? 'selected': NULL;
					$output .= "<option value='$id' $selected>$listDataValue</option>";
				}

				$output .= "</select>";

				break;

			case 'checkbox':
				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<input type='checkbox' name='$name' value='$value'>";

				break;
			case 'view':
				$output .= "<label for='$id' class='col-sm-2 control-label'>$label</label>
							<div class='col-sm-10'>";

				$output .= "<span $htmlOptions $htmlString>$value</span>";

				break;
			default:
				exit('Type not defined.');
		}

		$output .= "
				</div>
			</div>
			";

		return $output;
	}

	// Validate config field required
	public static function htmlInputValidate($config) {
		$fieldRequire = ['type', 'name', 'id', 'label'];

		foreach($fieldRequire as $value) {
			if (!isset($config[$value]))
				exit("Config field ($value) required not defined.");
		}
	}

	// Make HTML Tags
	public static function makeHTMLTags($data) {
		$ouput = NULL;

		foreach($data as $key => $value) {
			$ouput .= $key . "='" . $value . "' ";
		}

		return $ouput;
	}

	//
	public static function formButtons($config = []) {
		$output = "
			<div class='groupButtons'>
				<div id='buttonSave'>
					<button id='save' type='button' class='btn btn-primary'>Salvar</button>
				</div>

				<div id='buttonBack'>
					<button id='cancel' type='button' class='btn btn-default'>Voltar</button>
				</div>
			</div>
		";

        self::$outputJs .= "
            $('#buttonBack').click(function() {
                $('#buttonBack').attr('disabled', true);
                window.location = '/". $config['baseUrl'] ."';
            });
        ";

		return $output;
	}
	
	//
	public static function backButton($config = []) {
		$output = "
			<div class='groupButtons'>
				<div id='buttonBack'>
					<button id='cancel' type='button' class='btn btn-default'>Voltar</button>
				</div>
			</div>
		";

        self::$outputJs .= "
            $('#buttonBack').click(function() {
                $('#buttonBack').attr('disabled', true);
                window.location = '/". $config['baseUrl'] ."';
            });
        ";

		return $output;
	}

	//
	public static function formBegin(array $config) {
		$configDefault = [
			'id' => 'form1',
			'method' => 'post',
			'action' => '',
			'class' => '',
		];

		// O array do "usuario" tem mais prioridade que a configDefault.
		$config = self::array_merge_recursive_ex($configDefault, $config);

		extract($config);

		$output = "<form method='$method' action='$action' class='form-horizontal $class' id='$id'>";

		return $output;
	}

	//
	public static function formEnd() {
		$output = "</form>";

		return $output;
	}

	//
	public static function htmlTitle(array $config) {
		$content = $config['content'];

		return "<h1 class='title'>$content</h1>";
	}

	//
	public static function htmlSubTitle(array $config) {
		$content = $config['content'];

		return "<div class='subtitle'>$content</div>";
	}

	// by http://stackoverflow.com/questions/25712099/php-multidimensional-array-merge-recursive?answertab=active#tab-top
	public static function array_merge_recursive_ex(array & $array1, array & $array2) {
		$merged = $array1;

		foreach ($array2 as $key => & $value) {
			if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
				$merged[$key] = self::array_merge_recursive_ex($merged[$key], $value);
			}
			else
				if (is_numeric($key)) {
					if (!in_array($value, $merged))
						$merged[] = $value;
				}
				else
					$merged[$key] = $value;
		}

		return $merged;
	}
}
?>