<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends BaseModel
{
	protected $table = 'documento';
	protected $primaryKey = 'id_documento';
	protected $fillable = ['descricao', 'sigla'];
	
	// 
	public function getRules() {
		return [
				$this->getClassName() . '.descricao' => 'required',		
				$this->getClassName() . '.sigla' => 'required',		
			];
	}
	
	// 
	public $label = [
		'descricao' => 'Descrição',
		'sigla' 	=> 'Sigla',
	];
}