<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Redirect;
//use Session;
//use App\Documento;

class DocumentoController extends BaseController
{
	public $title = 'Documento';
	public $modelName = 'Documento';
	public $modelPath = '\App\Documento';
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		
		return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		return parent::store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {		
		return parent::update($request, $id);
    }	

    /**
     * Remove the selected resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAll()
    {
		return parent::destroyAll();
    }	
	
	// 
	public function grid_ajax() {
		$columns = [
			'id_documento',
			'descricao',
			'sigla',
		];	
		
		$orderType 	= $_GET['order'][0]['dir'];		
		$page 		= ($_GET['start'] / $_GET['length']) + 1;
		$search 	= $_GET['search']['value'];		
		$orderKey   = $_GET['order'][0]['column'];
		$orderField = $columns[$orderKey];
		
		$model = DB::table( $this->modelName );
		$recordsTotal = $model->count();

		foreach($columns as $column) {
			$modelsCondition = $model->orWhere( $column, 'like', "%$search%" );
		}
        
        $recordsFiltered = $modelsCondition->count();
		
		$models = $modelsCondition->orderBy($orderField, $orderType)->forPage( $page, $_GET['length'] )->get();
		
		$modelData = [];		
		foreach($models->toArray() as $data) {
			$newData = [];
			foreach($columns as $column)
				$newData[$column] = $data->$column;		
			
            $modelData[] = $newData;
		}
		
		$data = array(
					'draw' => $_GET['draw'],
					"recordsTotal" => $recordsTotal,
					"recordsFiltered" => $recordsFiltered,
					'data' => $modelData,
				);

		echo json_encode($data);
	}
}