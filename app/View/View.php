<?php
namespace App\View;

use App\View\View;
use App\Form\Form;

// 
class View {
	public $controller;
	public $errors;
	
	// 
	function __construct($config) {
		$this->controller = $config['controller'];
		$this->errors = $config['errors'];
   }
   
    // 
    public function getHeader() {
		echo Form::htmlTitle( ['content'=> $this->controller->title] );
		echo $this->controller->showErros($this->errors);
	}
	
	public function test() {
		ob_start();
		ob_implicit_flush(false);
		require('test.php');
		return ob_get_clean();
	}
}