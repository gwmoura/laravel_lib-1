@extends('layouts.app')

@section('content')
	<?= Form::htmlTitle( ['content'=> $controller->title] ) ?>

	<?
	$config = array(
				'label' => $model->label,
				//'data' => $data,
				
				// optional parameter
				'menu' => [
					['label' => 'Item 1', 'href' => 'item1'],
					['label' => 'Item 2', 'href' => 'item2'],
				],
				'htmlTable' => array(
					'id' => 'example',						
				),
			);		
	$grid = new Grid($config);				
	echo $grid->init();
	?>		
@endsection

@section('js')
	$( document ).ready(function() {
		config = {
			'id': '<?= $grid->id ?>', 
			'baseUrl': '<?= url( $controller->getControllerUri() . '/') ?>',
			'ajaxUrl': '<?= url( $controller->getControllerUri() . '/grid_ajax/') ?>',
			'columns': [
				{ "data": "id_documento" },
				{ "data": "descricao" },
				{ "data": "sigla" },
			],
			'order': {
				'column': 1,
				'type': 'desc'
			}
		}
		
		grid(config);
	});
@endsection	